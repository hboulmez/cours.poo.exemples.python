from unittest import TestCase, main
from coo import rectangle2dplein, point2d

class RectanglePleinTestCase(TestCase):
    def setUp(self):
        orig = point2d.Point2D(1.0, 2.0)
        self.r = rectangle2dplein.Rectangle2DPlein(orig, 3.0, 4.0, "rouge")

    def assert_rectangleplein(self, expectedOrigX, expectedOrigY, expectedFinX, expectedFinY, expectedCouleur):
        self.assertEqual(self.r.orig.x, expectedOrigX)
        self.assertEqual(self.r.orig.y, expectedOrigY)
        self.assertEqual(self.r.fin.x, expectedFinX)
        self.assertEqual(self.r.fin.y, expectedFinY)
        self.assertEqual(self.r.couleur, expectedCouleur)

    def test_constructor(self):
        self.assert_rectangleplein(1.0, 2.0, 4.0, 6.0, "rouge")

    def test_str(self):
        self.assertEqual(str(self.r), "Rectangle2DPlein(Point2D(1.0, 2.0), Point2D(4.0, 6.0), rouge)")

if __name__ == '__main__':
    main()
