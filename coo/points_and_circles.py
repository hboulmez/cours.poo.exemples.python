#!/usr/bin/env python3

"""Exemples simples de création d'objets"""

from coo import point2d, cercle2d

if __name__ == '__main__':
    # tag::points_and_circles[]
    point1 = point2d.Point2D(1.0, 2.0)
    point2 = point2d.Point2D(1.0, 2.0)
    cercle1 = cercle2d.Cercle2D(point1, 2.0)
    # end::points_and_circles[]

    print("point1 -> {}".format(point1))
    print("point2 -> {}".format(point2))
    print("cercle1 -> {}".format(cercle1))

    # tag::moving_circle[]
    cercle1.translate(2.0, 3.0)
    # end::moving_circle[]

    print("point1 -> {}".format(point1))
    print("point2 -> {}".format(point2))
    print("cercle1 -> {}".format(cercle1))
