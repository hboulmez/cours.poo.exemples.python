"""Définition de la classe Point2D"""


class Point2D(object):
    """Cette classe représente un point en 2 dimensions"""

    def __init__(self, x, y):
        """Initialise un point à partir d'un abscisse et d'une ordonnée"""
        self.x = x
        self.y = y

    def __str__(self):
        return "Point2D({}, {})".format(self.x, self.y)

    def translate(self, dx, dy):
        """Déplace le point"""
        self.x += dx
        self.y += dy
